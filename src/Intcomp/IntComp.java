package Intcomp;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Scanner;

public class IntComp {

    public static void main(String[] args) {

        int expected = 19690720;
        int noun = 0;
        int verb = 0;
        int input = 1;
        int output;


        String line = "";
        File file = new File("/home/dw/IdeaProjects/Advent/src/Intcomp/data.txt");
        try {
            Scanner sc = new Scanner(file);
            line = sc.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String[] tabOfStrings;
        tabOfStrings = line.split(",");
        int[] tab = new int[tabOfStrings.length];
        for(int i = 0; i < tabOfStrings.length; i++){
            tab[i] = Integer.valueOf(tabOfStrings[i]);
        }
        System.out.println(tab.length);
        System.out.println(tabOfStrings.length);

        int[] copy = new int[tab.length];

        for (int i = 0; i < tab.length; i++) {
            copy[i] = tab[i];
        }
        boolean stop = false;

        while (!stop) {
            if (tab[0] == expected) {
                stop = true;
            }

            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {

                    tab[1] = i;
                    tab[2] = j;
                    int code = tab[0];
                    int value = 0;
                    int arg1Index;
                    int arg2Index;
                    int valueIndex;
                    int index = 0;

                    while (code != 99) {
                        code = tab[index];
                        arg1Index = tab[index + 1];
                        arg2Index = tab[index + 2];
                        valueIndex = tab[index + 3];
                        if ((arg1Index > tab.length - 2) || (arg2Index > tab.length - 2) || (valueIndex > tab.length - 2)) {
                            stop = true;
                            break;
                        }
                        if (code == 1) {
                            value = tab[arg1Index] + tab[arg2Index];
                            tab[valueIndex] = value;
                        }
                        if (code == 2) {
                            value = tab[arg1Index] * tab[arg2Index];
                            tab[valueIndex] = value;
                        }
                        index += 4;
                    }
//                    System.out.println(tab[0]);
//                    System.out.println(expected);
//                    System.out.print("noun: " + i);
//                    System.out.println("   verb: " + j);

                    for (int k = 0; k < tab.length; k++) {
                        tab[k] = copy[k];
                    }
                }
            }
        }
    }

    public int decoder(int[] tab, int input ){
        int index = 0;
        int arg1Index;
        int arg2Index;
        int valueIndex;
        int code = tab[index];
        int value = 0;
        boolean stop;

        while (code != 99) {
            code = tab[index];
            arg1Index = tab[index + 1];
            arg2Index = tab[index + 2];
            valueIndex = tab[index + 3];
            if ((arg1Index > tab.length - 2) || (arg2Index > tab.length - 2) || (valueIndex > tab.length - 2)) {
                stop = true;
                break;
            }
            if (code == 1) {
                value = tab[arg1Index] + tab[arg2Index];
                tab[valueIndex] = value;
            }
            if (code == 2) {
                value = tab[arg1Index] * tab[arg2Index];
                tab[valueIndex] = value;
            }
            index += 4;
        }

        return 0;
    }
}
