package Intcomp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class IntComp2 {
    public static void main(String[] args) {

        int input = 5;
        int output;

        String line = "";
        File file = new File("/home/dw/IdeaProjects/Advent/src/Intcomp/data.txt");
        //File file = new File("/home/dw/IdeaProjects/Advent/src/Intcomp/test.txt");
        try {
            Scanner sc = new Scanner(file);
            line = sc.nextLine();
            System.out.println(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String[] tabOfStrings;
        tabOfStrings = line.split(",");
        int[] tab = new int[tabOfStrings.length];
        for (int i = 0; i < tabOfStrings.length; i++) {
            tab[i] = Integer.valueOf(tabOfStrings[i]);
        }
        System.out.println("Rozmiar listy komend: " + tab.length);
        System.out.println("tab[225] = " + tab[225]);

        output = decoder(tab, input);
        System.out.println("Output: " + output);

        System.out.println("Memory after stop program: " + Arrays.toString(tab));
    }

    public static int decoder(int[] tab, int input) {
        int index = 0;
        int arg1Index;
        int arg1Value = 0;
        int arg2Index;
        int arg2Value = 0;
        int arg3Index;
        int arg3Value = 0;
        int valueIndex;
        int code = tab[index];
        int value = 0;
        int output = 0;
        int[] multipleTab = new int[4];

        boolean multiply = false;

        while (code != 99) {
            if (code == 1) {                    //Add
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                valueIndex = tab[index + 3];
                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (valueIndex > tab.length - 1)) {
                    break;
                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }

                value = arg1Value + arg2Value;
                tab[valueIndex] = value;
                index += 4;
                code = tab[index];
                clearMultiplyTab(multipleTab);
            }
            if (code == 2) {                    //Multiply
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                valueIndex = tab[index + 3];
                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (valueIndex > tab.length - 1)) {
                    break;
                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }

                value = arg1Value * arg2Value;
                tab[valueIndex] = value;
                index += 4;
                code = tab[index];
                clearMultiplyTab(multipleTab);

            }
            if (code == 3) {                    //Input
                valueIndex = tab[index + 1];
                tab[valueIndex] = input;
                index += 2;
                code = tab[index];
            }
            if (code == 4) {                    //Output
                valueIndex = tab[index + 1];
                output = tab[valueIndex];
                index += 2;
                code = tab[index];
            }

            if (code == 5) {                    //jump if true
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                valueIndex = tab[index + 3];
//                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (valueIndex > tab.length - 1)) {
//                    break;
//                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }

                if (arg1Value != 0) {
                    index = arg2Value;
                } else
                    index += 3;
                code = tab[index];
                clearMultiplyTab(multipleTab);
            }

            if (code == 6) {                    //jump if false
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                valueIndex = tab[index + 3];
//                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (valueIndex > tab.length - 1)) {
//                    break;
//                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }

                if (arg1Value == 0) {
                    index = arg2Value;
                } else
                    index += 3;
                code = tab[index];
                clearMultiplyTab(multipleTab);
            }

            if (code == 7) {                    //less
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                arg3Index = tab[index + 3];
//                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (arg3Index > tab.length - 1)) {
//                    break;
//                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }
                if (multipleTab[3] == 0) {
                    arg3Value = tab[arg3Index];
                }
                if (multipleTab[3] == 1) {
                    arg3Value = arg3Index;
                }

                if ((arg1Value < arg2Value) && multipleTab[3] == 0) {  //position mode
                    tab[arg3Index] = 1;
                } else {
                    tab[arg3Index] = 0;
                }

                if ((arg1Value < arg2Value) && multipleTab[3] == 1) {  //immediate mode
                    tab[arg3Value] = 1;
                } else {
                    tab[arg3Value] = 0;
                }

                index += 4;
                code = tab[index];
                clearMultiplyTab(multipleTab);
            }


            if (code == 8) {                    //equals
                arg1Index = tab[index + 1];
                arg2Index = tab[index + 2];
                arg3Index = tab[index + 3];
                if ((arg1Index > tab.length - 1) || (arg2Index > tab.length - 1) || (arg3Index > tab.length - 1)) {
                    break;
                }

                if (multipleTab[1] == 0) {
                    arg1Value = tab[arg1Index];
                }
                if (multipleTab[1] == 1) {
                    arg1Value = arg1Index;
                }
                if (multipleTab[2] == 0) {
                    arg2Value = tab[arg2Index];
                }
                if (multipleTab[2] == 1) {
                    arg2Value = arg2Index;
                }

                if (multipleTab[3] == 0) {
                    arg3Value = tab[arg3Index];
                }
                if (multipleTab[3] == 1) {
                    arg3Value = arg3Index;
                }

                if ((arg1Value == arg2Value) && multipleTab[3] == 0) {  //position mode
                    tab[arg3Index] = 1;
                } else {
                    tab[arg3Index] = 0;
                }

                if ((arg1Value == arg2Value) && multipleTab[3] == 1) {  //immediate mode
                    tab[arg3Value] = 1;
                } else {
                    tab[arg3Value] = 0;
                }


//                value = arg1Value * arg2Value;
//                tab[valueIndex] = value;

                index += 4;
                code = tab[index];
                clearMultiplyTab(multipleTab);

            }

            if (code > 99) {                                 //limit komend!
                multiply = true;
                String s = String.valueOf(code);
                if (s.length() == 4) {
                    s = "0" + s;
                }
                if (s.length() == 3) {
                    s = "00" + s;
                }
                System.out.println("String: " + s);
                multipleTab[0] = Integer.valueOf(s.substring(3, 5));
                multipleTab[1] = s.charAt(2) - 48;
                multipleTab[2] = s.charAt(1) - 48;
                multipleTab[3] = s.charAt(0) - 48;

                System.out.println(Arrays.toString(multipleTab));

                code = multipleTab[0];
                System.out.println("Zdekodowana komenda: " + code);

            }
            if ((code > 8 && code < 99) || (code < 1)) {
                continue;
            }
            if (code == 99) {
                break;
            }
            System.out.println("index: " + index);
        }
        return output;
    }

    public static void clearMultiplyTab(int[] multi) {
        for (int i = 0; i < 4; i++) {
            multi[i] = 0;
        }
    }
}
