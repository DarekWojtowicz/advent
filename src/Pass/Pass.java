package Pass;

import java.util.ArrayList;
import java.util.List;

public class Pass {

    static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) {
        int start = 153517;
        int stop = 630395;
        int size = stop - start + 1;


        int[] possiblePass = new int[size];
        for (int i = 0; i < size; i++) {
            possiblePass[i] = start;
            checker(start);
            start++;
        }

        System.out.println(possiblePass.length);
        System.out.println(list.size());
        System.out.println(list.toString());
    }

    public static void checker(int value) {

        String sv = String.valueOf(value);
        int a1, a2, a3, a4, a5, a6;
        a1 = Integer.valueOf(sv.charAt(0) - 48);
        a2 = Integer.valueOf(sv.charAt(1) - 48);
        a3 = Integer.valueOf(sv.charAt(2) - 48);
        a4 = Integer.valueOf(sv.charAt(3) - 48);
        a5 = Integer.valueOf(sv.charAt(4) - 48);
        a6 = Integer.valueOf(sv.charAt(5) - 48);
        if (sv.matches("[134567890]*(22)[134567890]*")) {
            if (a1 <= a2 && a2 <= a3 && a3 <= a4 && a4 <= a5 && a5 <= a6) {
                list.add(a1 * 100000 + a2 * 10000 + a3 * 1000 + a4 * 100 + a5 * 10 + a6);
            }
        }
    }
}
